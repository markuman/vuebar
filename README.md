# vuebar

Just put the index.html somewhere and browse it.

# dynamic data

# vuebar

add e.g. `https://markuman.gitlab.io/vuepie/?data={"Linux":64,"Mac":35,"Windos":1}` to an url.

# git mirror

- https://gitlab.com/markuman/vuebar
- https://github.com/markuman/vuebar

# deployment mirrors

- https://markuman.gitlab.io/vuebar

# made with

- [vuejs](https://vuejs.org/)
- [vue-chartkick](https://github.com/ankane/vue-chartkick)
